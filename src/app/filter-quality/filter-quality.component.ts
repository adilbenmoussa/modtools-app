import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-filter-quality',
  template: `<router-outlet></router-outlet>`,
})
export class FilterQualityComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
