import {ActivatedRoute, Event, NavigationEnd, PRIMARY_OUTLET} from '@angular/router';
import {Observable} from 'rxjs';
import {filter, map, mergeMap} from 'rxjs/operators';

/**
 * Gets the passed route data from the routing configuration.
 * Credits goes to BreadcrumbsComponent for this part of the code
 * 
 * @param events - Route events
 * @param activatedRoute -  the current activated route
 * 
 * @returns Stream of the configured data or undefined
 */

export function lookUpPrimaryOutletData(events: Observable<Event>, activatedRoute: ActivatedRoute): Observable<any> {
  return events
    .pipe(filter((event) => event instanceof NavigationEnd))
    .pipe(map(() => activatedRoute))
    .pipe(map((route) => {
      while (route.firstChild) route = route.firstChild;
      return route;
    }))
    .pipe(filter((route) => route.outlet === PRIMARY_OUTLET))
    .pipe(mergeMap((route) => route.data));
}
