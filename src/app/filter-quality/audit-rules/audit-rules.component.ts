import {Component, OnDestroy, OnInit} from '@angular/core';
import {switchMap, tap, delay} from 'rxjs/operators';
import {SearchService} from '../../main/search/search.service';
import {AuditRulesService} from './audit-rules.service';
import {EnrichedTextClassifiedOutput, TextInput} from '../filter-quality-interfaces';
import {UserService} from '../../shared-components/user.service';
import {Unsubscribable} from '../../decorators/unsubscribable.decorator';

@Component({
  selector: 'app-audit-rules',
  templateUrl: './audit-rules.component.html',
  styleUrls: ['./audit-rules.component.less']
})
@Unsubscribable()
export class AuditRulesComponent implements OnInit, OnDestroy {
  public textClassifiedOutput: EnrichedTextClassifiedOutput;
  isLoading = false;

  constructor(private auditRulesService: AuditRulesService,
              private searchService: SearchService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.searchService
      .refreshClickedOrSearched$
      .pipe(
        tap(() => {
          this.textClassifiedOutput = null;
          this.isLoading = true;
        }),
        delay(400), // delays the call, in order to show the loading state
        switchMap((text: string) => {
          const isDeeperAnalysis = this.searchService.deeperAnalysis$.getValue();
          const prefs = this.userService.preferences;
          const body: TextInput = {
            text,
            clientId: prefs.lastClientId,
            language: prefs.language,
            contentType: prefs.contentType
          };
          return this.auditRulesService.classifyText(body, isDeeperAnalysis)
        })
      )
      .subscribe(result => {
        this.textClassifiedOutput = result;
        this.isLoading = false
      })
      .unsubscribeOnDestroy(this);
  }

  ngOnDestroy(): void {
  }
}
