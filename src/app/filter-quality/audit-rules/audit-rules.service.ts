import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {EnrichedTextClassifiedOutput, TextClassifiedOutput, TextInput} from '../filter-quality-interfaces';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {CustomHttpUrlEncodingCodec} from '../../utils/encoder.utils';
import {AuditRulesUtilNamespace as auditRulesUtil} from '../../utils/audit-rules-util';
import {HttpUtilNamespace as httpUtil} from '../../utils/http-utils';

@Injectable({
  providedIn: 'root'
})
export class AuditRulesService {

  constructor(private httpClient: HttpClient) {
  }

  classifyText(body?: TextInput, extended?: boolean): Observable<EnrichedTextClassifiedOutput> {

    let params;
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    if (extended !== undefined && extended !== null) {
      params = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()})
        .set('extended', extended.toString());
    }

    // When extended is falsy we dont mind if we send undefined params
    // object since it is optional, so it will be ignored
    return this.httpClient
      .post<TextClassifiedOutput>(`${environment.apiBaseUrl}/classify/text`, body, {headers, params})
      .pipe(
        map((result: TextClassifiedOutput) => {
          // console.log('auditRulesUtil.enrichTextClassifiedOutput(result);', auditRulesUtil.enrichTextClassifiedOutput(result));
          return auditRulesUtil.enrichTextClassifiedOutput(result);
        }),
        catchError(httpUtil.handleError<TextClassifiedOutput>('classifyText'))
      );
  }
}
