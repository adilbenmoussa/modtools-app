import {Subscription} from 'rxjs';
import {unsubscribeOnDestroy} from './unsubscribe-on-destroy.operator';

describe('unsubscribeOnDestroy', () => {

  it('should throw an error when the context is not provided', () => {
    const errorMessage = 'Subscription is using unsubscribeOnDestroy but does not pass the correct context';
    spyOn(console, 'error');

    unsubscribeOnDestroy(null);

    expect(console.error).toHaveBeenCalledWith(errorMessage);
  });

  it('should add a disposeBag to the component if not available', () => {
    const component = {
      disposeBag: null
    };
    const executionContext = {};
    unsubscribeOnDestroy.bind(executionContext)(component);

    expect(component.disposeBag).toBeDefined();
  });

  it('should add the subscriptionContext to the disposeBag list', () => {
    const component = {
      disposeBag: null
    };
    const subscriptionContext = new Subscription();
    unsubscribeOnDestroy.bind(subscriptionContext)(component);

    expect(component.disposeBag).toEqual([subscriptionContext]);
  });

  it('should use the disposeBag of the component if available', () => {
    const component = {
      disposeBag: [new Subscription()]
    };
    const subscriptionContext = new Subscription();
    unsubscribeOnDestroy.bind(subscriptionContext)(component);

    expect(component.disposeBag.length).toEqual(2);
  });
});


