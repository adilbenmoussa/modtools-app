import {Component, Input, EventEmitter, Output, OnInit} from '@angular/core';
import {faChevronDown} from '@fortawesome/free-solid-svg-icons';
import {head} from 'lodash';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.less']
})
export class DropdownComponent implements OnInit {
  faChevronDown = faChevronDown;

  @Input() dataProvider?: any[];
  @Input() selectedItem?: any;
  @Input() selectedItemKey = 'name';
  @Output() selectedItemChanged = new EventEmitter<any>();

  onSelectedItemChanged(item: any) {
    this.selectedItem = item;
    this.selectedItemChanged.emit(item);
    console.log('selectedItem', item);
  }

  ngOnInit(): void {
    this.selectedItem = this.dataProvider && head(this.dataProvider);
  }
}
