import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { BreadcrumbModule } from 'xng-breadcrumb';

import { GenericViewComponent } from './generic-view.component';
import { GenericViewRoutingModule } from './generic-view-routing.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import {ClientPickerComponent} from '../main/client-picker/client-picker.component';
import {LanguagePickerComponent} from '../main/language-picker/language-picker.component';
import {ContentTypePickerComponent} from '../main/content-type-picker/content-type-picker.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { FilterPaneComponent } from './filter-pane/filter-pane.component';

@NgModule({
  declarations: [
    ClientPickerComponent,
    LanguagePickerComponent,
    ContentTypePickerComponent,
    GenericViewComponent,
    FilterPaneComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    GenericViewRoutingModule,
    SharedComponentsModule,
    BreadcrumbModule,
  ]
})
export class GenericViewModule { }
