import {Component, Input, OnInit} from '@angular/core';
import {faComments, faAngry, IconDefinition} from '@fortawesome/free-solid-svg-icons';
import {findIconDefinition, icon as fortawesomeIcon, SizeProp} from '@fortawesome/fontawesome-svg-core'
import {ChatView, EnrichedTopics} from '../../../filter-quality-interfaces';

export type TopicViewMode = 'Topics' | 'PolicyGuides';

@Component({
  selector: 'app-topic-view',
  templateUrl: './topic-view.component.html',
  styleUrls: ['./topic-view.component.less']
})
export class TopicViewComponent implements OnInit {
  @Input() enrichedTopics?: EnrichedTopics;
  @Input() chatView?: ChatView;
  @Input() mode: TopicViewMode;
  name: string;
  id: string;
  class: string;
  icon: IconDefinition;
  size: SizeProp = 'sm';

  iconTopics(): IconDefinition {
    switch (this.enrichedTopics.topicRate.id) {
      case 2:
        return faComments;
      case 5:
        return faAngry;
    }
    return null;
  }

  ngOnInit() {
    if (this.mode === 'Topics') {
      this.name = this.enrichedTopics.riskLevel.name;
      this.class = this.enrichedTopics.topicRate.class;
      this.id = `${this.enrichedTopics.topicRate.id}`;
      this.icon = this.iconTopics();
    } else if (this.mode === 'PolicyGuides') {
      this.name = this.chatView.name;
      this.class = this.chatView.class;
      this.size = 'lg';
      this.icon = fortawesomeIcon(findIconDefinition({
        prefix: 'fas',
        iconName: this.chatView.icon
      }));
    }
  }
}
