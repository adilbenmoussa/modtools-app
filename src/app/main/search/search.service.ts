import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable,} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, switchMap} from 'rxjs/operators';

@Injectable()
export class SearchService {
  private _searchText$ = new BehaviorSubject<string>('');
  private _refreshClick$ = new BehaviorSubject<Event>(null);
  public deeperAnalysis$ = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  search(typedText: string) {
    this._searchText$.next(typedText);
  }

  get refreshClickedOrSearched$(): Observable<string> {
    return this._refreshClick$
      .pipe(
        switchMap(() => {
          return this._searchText$
            .pipe(
              filter(value => !!value),
              debounceTime(300),
              distinctUntilChanged()
            );
        })
      )
  }

  refreshSearch(event) {
    this._refreshClick$.next(event);
  }


  toggleDeepAnalysis() {
    this.deeperAnalysis$.next(!this.deeperAnalysis$.getValue());
  }
}
