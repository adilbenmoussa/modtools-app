import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {faChevronUp, faChevronDown} from '@fortawesome/free-solid-svg-icons';
import {sortBy} from 'lodash';

import {Languages} from 'src/constants';
import {UserService} from 'src/app/shared-components/user.service';
import {LanguageEnum} from '../../filter-quality/filter-quality-interfaces';

@Component({
  selector: 'app-language-picker',
  templateUrl: './language-picker.component.html',
  styleUrls: ['./language-picker.component.less']
})
export class LanguagePickerComponent implements OnInit {
  faChevronDown = faChevronDown;

  @Input() selectedLanguage: LanguageEnum = 'en';
  @Output() languageChanged = new EventEmitter<LanguageEnum>();

  languages = [];

  constructor(private userService: UserService) {
  }

  async ngOnInit() {

    // Get the current user
    const user = await this.userService.me();

    // Filter the user's languages by the ones they're allowed to use
    if (user?.config?.allowedLanguages) {
      this.languages = Languages.filter(lang => user.config.allowedLanguages.includes(lang.code));
    }

    // Sort by language name, not code.
    this.languages = sortBy(this.languages, 'name');
  }

  /**
   * Fired when the user changes their language
   */
  onLanguageChanged() {
    this.languageChanged.emit(this.selectedLanguage);
  }

}
