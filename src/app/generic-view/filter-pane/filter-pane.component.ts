import {Component, OnInit} from '@angular/core';
import {faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {UserService} from '../../shared-components/user.service';
import {SearchService} from '../../main/search/search.service';
import {Client, Clients, ContentType, ContentTypes, Language, Languages} from '../../../constants';
import {sortBy} from 'lodash';

@Component({
  selector: 'app-filter-pane',
  templateUrl: './filter-pane.component.html',
  styleUrls: ['./filter-pane.component.less']
})
export class FilterPaneComponent implements OnInit {
  faSyncAlt = faSyncAlt;
  selectedLanguage: Language;
  languages = [];
  selectedClient: Client;
  clients: Client[] = [];

  selectedContentType: ContentType;
  contentTypes: ContentType[];

  constructor(public searchService: SearchService,
              public userService: UserService) {
  }

  async ngOnInit() {

    // Get the current user
    const user = await this.userService.me();

    // Filter the user's languages by the ones they're allowed to use
    if (user?.config?.allowedLanguages) {
      this.languages = Languages.filter(lang => user.config.allowedLanguages.includes(lang.code));
    }
    // Filter the user's languages by the ones they're allowed to use
    if (user?.config?.allowedClients) {
      this.clients = Clients.filter(lang => user.config.allowedClients.includes(lang.id));
    }

    this.selectedLanguage = this.languages.find(({code}) => code === this.userService.preferences.language) || this.languages[0];
    this.languages = sortBy(this.languages, 'name');

    this.selectedClient = this.clients.find(({id}) => id === this.userService.preferences.lastClientId) || this.clients[0];
    this.clients = sortBy(this.clients, 'name');

    this.contentTypes = sortBy(ContentTypes, 'name');
    this.selectedContentType = this.contentTypes.find(({id}) => id === this.userService.preferences.contentType) || this.contentTypes[0];
  }

  onUserChangedClient(newClient: Client) {
    this.userService.updatePreference({
      lastClientId: newClient.id
    });
  }

  onUserChangedLanguage(newLanguage: Language) {
    this.userService.updatePreference({
      language: newLanguage.code
    });
  }

  onUserChangedContentType(newContentType: ContentType) {
    this.userService.updatePreference({
      contentType: newContentType.id
    });
  }
}
