import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as Sentry from '@sentry/browser';

import {User, UserPreferences, UserPreferencesKey} from './user';
import {DefaultLanguage, DefaultClient, Languages, Clients, DefaultContentType} from 'src/constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  defaultPrefs: UserPreferences = {
    language: DefaultLanguage,
    lastClientId: DefaultClient,
    contentType: DefaultContentType
  };

  defaultUser: User = {
    moderatorId: 'user@example.com',
    displayName: 'DEFAULT USER',
    email: 'user@example.com',
    emails: [{value: 'user@example.com', verified: false}],
    id: 'user@example.com',
    name: {familyName: 'User', givenName: 'Default'},
    photos: [],
    provider: 'NONE',
    config: {
      allowedLanguages: ['ar', 'en', 'fr', 'nl'], // just add all languages for now
      allowedClients: [60, 61]
    }
  };
  prefs: UserPreferences;
  user: User;

  constructor(private http: HttpClient) {
    // persist default preferences
    this.persist('prefs', this.defaultPrefs);
  }

  async me(): Promise<User> {

    // If we already have a user, don't get it again
    if (this.user) return this.user;

    try {

      this.user = this.defaultUser;

      // this.user = await this.http.get<User>('/me').toPromise();

      // // Set the user in Sentry
      // Sentry.setUser({
      //   email: this.user?.email,
      //   name: this.user?.displayName
      // });

    } catch (error) {

      // If we failed to get a user, return a default one.
      if (error.status === 404) this.user = this.defaultUser;

      // Unset the Sentry user
      Sentry.setUser(null);

    }

    return this.user;

  }

  get preferences(): UserPreferences {

    // Use in-memory prefs first
    if (this.prefs) return this.prefs;

    // Try getting prefs from LocalStorage
    if ('localStorage' in window) {
      const savedPrefs: string = window.localStorage.getItem('prefs');
      if (savedPrefs) {
        try {

          this.prefs = JSON.parse(savedPrefs);
          return this.prefs;

        } catch (error) {
          Sentry.addBreadcrumb({
            category: 'user',
            message: 'Tried loading user prefs from LocalStorage',
            level: Sentry.Severity.Error
          });
          Sentry.captureException(error);
          console.error(error);
        }
      }
    }

    // Fail back to default prefs
    return this.defaultPrefs;
  }

  updatePreference(partialPreference: Partial<UserPreferences>) {
    // Save to memory
    this.prefs = {
      ...this.prefs,
      ...partialPreference
    };

    this.persist('prefs', this.prefs);
  }

  persist(key: string, value: any) {
    // Serialize and save to LocalStorage
    if ('localStorage' in window) {

      try {
        window.localStorage.setItem(key, JSON.stringify(value));

      } catch (error) {
        Sentry.addBreadcrumb({
          category: 'user',
          message: `Tried saving user ${key} to LocalStorage`,
          level: Sentry.Severity.Error
        });
        Sentry.captureException(error);
        console.error(error);
      }
    }
  }
}
