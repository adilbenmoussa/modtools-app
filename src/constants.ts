import {ContentTypeEnum, EnrichedTopicRateEnum, LanguageEnum} from './app/filter-quality/filter-quality-interfaces';

export class TopicsEnum {
  public static 0: EnrichedTopicRateEnum = {name: 'General Risk', id: 0, class: '--general'};
  public static 1: EnrichedTopicRateEnum = {name: 'Bullying', id: 1, class: '--bullying'};
  public static 2: EnrichedTopicRateEnum = {name: 'Fighting', id: 2, class: '--fighting'};
  public static 3: EnrichedTopicRateEnum = {name: 'Personal Information', id: 3, class: '--personal-information'};
  public static 4: EnrichedTopicRateEnum = {name: 'Dating and Sexting', id: 4, class: '--dating-and-sexting'};
  public static 5: EnrichedTopicRateEnum = {name: 'Vulgar', id: 5, class: '--vulgar'};
  public static 6: EnrichedTopicRateEnum = {name: 'Drugs and Alcohol', id: 6, class: '--drugs-and-alcohol'};
  public static 7: EnrichedTopicRateEnum = {name: 'In-Game Items', id: 7, class: '--ing-game-items'};
  public static 8: EnrichedTopicRateEnum = {name: 'Alarm', id: 8, class: '--alarm'};
  public static 9: EnrichedTopicRateEnum = {name: 'Fraud', id: 9, class: '--fraud'};
  public static 10: EnrichedTopicRateEnum = {name: 'Racist', id: 10, class: '--racist'};
  public static 11: EnrichedTopicRateEnum = {name: 'Religion', id: 11, class: '--religion'};
  public static 13: EnrichedTopicRateEnum = {name: 'Website', id: 13, class: '--website'};
  public static 14: EnrichedTopicRateEnum = {name: 'Grooming', id: 14, class: '--grooming'};
  public static 15: EnrichedTopicRateEnum = {name: 'Public Threats', id: 15, class: '--public-threats'};
  public static 16: EnrichedTopicRateEnum = {name: 'Real Name', id: 16, class: '--real-name'};
  public static 17: EnrichedTopicRateEnum = {name: 'Radicalization',id: 17, class: '--radicalization'};
  public static 18: EnrichedTopicRateEnum = {name: 'Subversive',id: 18, class: '--subversive'};
  public static 19: EnrichedTopicRateEnum = {name: 'Sentiment', id: 19, class: '--sentiment'};
  public static 20: EnrichedTopicRateEnum = {name: 'Politics', id: 20, class: '--politics'};
  public static 27: EnrichedTopicRateEnum = {name: 'Custom1', id: 27, class: '--custom1'};
  public static 28: EnrichedTopicRateEnum = {name: 'Custom2', id: 28, class: '--custom2'};
  public static 29: EnrichedTopicRateEnum = {name: 'Custom3', id: 29, class: '--custom3'};
  public static 30: EnrichedTopicRateEnum = {name: 'Custom4', id: 30, class: '--custom4'};
  public static 31: EnrichedTopicRateEnum = {name: 'Custom5', id: 31, class: '--custom5'};
}

export type TextRiskLevelClassificationEnum =
  'SUPERSAFE' |
  'WHITELISTED' |
  'GREY' |
  'QUESTIONABLE' |
  'UNKNOWN' |
  'MILD' |
  'OFFENSIVE' |
  'OBSCENE';

export interface TextRiskLevelClassification {
  name: TextRiskLevelClassificationEnum;
  class: string;
  id: number;
}

export const TextRiskLevelClassifications: TextRiskLevelClassification[] = [
  {
    id: 0,
    name: 'SUPERSAFE',
    class: '--supersafe'
  },
  {
    id: 1,
    name: 'WHITELISTED',
    class: '--whitelisted'
  },
  {
    id: 2,
    name: 'QUESTIONABLE',
    class: '--questionable'
  },
  {
    id: 3,
    name: 'UNKNOWN',
    class: '--unknown'
  },
  {
    id: 4,
    name: 'MILD',
    class: '--mild'
  },
  {
    id: 5,
    name: 'OFFENSIVE',
    class: '--offensive'
  },
  {
    id: 6,
    name: 'OBSCENE',
    class: '--obscene'
  }
];

export interface ContentType {
  id: ContentTypeEnum;
  name: string;
}

export const ContentTypes: ContentType[] = [
  {id: 'SHORT_TEXT', name: 'Chat'},
  {id: 'LONG_TEXT', name: 'Text'},
  {id: 'USERNAME', name: 'Username'},
];

export interface Client {
  id: number;
  name: string;
}

export const Clients: Client[] = [
  {id: 60, name: 'Live'},
  {id: 61, name: 'Sandbox'},
  {id: 0, name: 'Community Sift'}
];

export interface Language {
  code: LanguageEnum;
  name: string;
  partial?: boolean;
}

export const Languages: Language[] = [
  {code: 'ar', name: 'Arabic', partial: false},
  {code: 'da', name: 'Danish', partial: true},
  {code: 'de', name: 'German', partial: false},
  {code: 'en', name: 'English', partial: false},
  {code: 'es', name: 'Spanish', partial: false},
  {code: 'fi', name: 'Finnish', partial: false},
  {code: 'fr', name: 'French', partial: false},
  {code: 'hi', name: 'Hindi', partial: true},
  {code: 'id', name: 'Indonesian', partial: false},
  {code: 'it', name: 'Italian', partial: false},
  {code: 'ja', name: 'Japanese', partial: false},
  {code: 'ko', name: 'Korean', partial: false},
  {code: 'nl', name: 'Dutch', partial: false},
  {code: 'no', name: 'Norwegian', partial: true},
  {code: 'pl', name: 'Polish', partial: false},
  {code: 'pt', name: 'Portuguese', partial: false},
  {code: 'ro', name: 'Romanian', partial: false},
  {code: 'ru', name: 'Russian', partial: false},
  {code: 'sv', name: 'Swedish', partial: true},
  {code: 'th', name: 'Thai', partial: false},
  {code: 'tr', name: 'Turkish', partial: false},
  {code: 'vi', name: 'Vietnamese', partial: false},
  {code: 'zh', name: 'Chinese', partial: false},
];

// Set the default client
export const DefaultClient = 60;

// Set the default language to English
export const DefaultLanguage = 'en';

// Set the default content type to SHORT_TEXT
export const DefaultContentType = 'SHORT_TEXT';
