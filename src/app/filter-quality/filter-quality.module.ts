import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {BreadcrumbModule} from 'xng-breadcrumb';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import {SharedComponentsModule} from '../shared-components/shared-components.module';
import {FilterQualityComponent} from './filter-quality.component';
import {AuditRulesComponent} from './audit-rules/audit-rules.component';
import {FilterQualityRoutingModule} from './filter-quality-routing.module';
import {AuditRulesService} from './audit-rules/audit-rules.service';
import {WordsMatrixComponent} from './audit-rules/words-matrix/words-matrix.component';
import {AnalysisComponent} from './audit-rules/analysis/analysis.component';
import {WordsMatrixDropdownComponent} from './audit-rules/words-matrix/words-matrix-dropdown/words-matrix-dropdown.component';
import {TopicViewComponent} from './audit-rules/analysis/topic-view/topic-view.component';
import {AiPredictionViewComponent} from './audit-rules/analysis/ai-prediction-view/ai-prediction-view.component';


@NgModule({
  declarations: [
    AiPredictionViewComponent,
    TopicViewComponent,
    WordsMatrixDropdownComponent,
    WordsMatrixComponent,
    AnalysisComponent,
    FilterQualityComponent,
    AuditRulesComponent,
  ],
  imports: [
    FontAwesomeModule,
    FilterQualityRoutingModule,
    CommonModule,
    SharedComponentsModule,
    BreadcrumbModule,
    RouterModule,
  ],
  providers: [
    AuditRulesService
  ]
})
export class FilterQualityModule {
}
