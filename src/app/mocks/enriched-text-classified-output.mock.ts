import {EnrichedTextClassifiedOutput} from '../filter-quality/filter-quality-interfaces';

export function enrichedTextClassifiedOutputMock(): EnrichedTextClassifiedOutput {
  return JSON.parse(`
  {
  "failingFragments": [
    {
      "endPos": 52,
      "normalized": "shit",
      "redactedText": "",
      "startPos": 44,
      "text": "shit",
      "topics": {
        "0": 5,
        "5": 5
      }
    }
  ],
  "language": "en",
  "predictions": {
    "lang_en": 0.9800100136,
    "lang_fr": 0.0190100136,
    "topic_fraud": 0.10001
  },
  "pseudonmized": false,
  "simplified": "i really love {{breakfast_food}} they are the shit",
  "text": "I really love scrambled eggs.  They are the Shiiiiit.",
  "topics": {
    "0": 5,
    "4": 2,
    "5": 5
  },
  "extended": [
    {
      "original": "I",
      "text": "i",
      "solution": "i",
      "tokens": [
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "i",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{alphabet}}",
            "ï"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{alphabet}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "ï",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "i"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{pronoun}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "really",
      "text": "really",
      "solution": "really",
      "tokens": [
        {
          "text": "really",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{intensifier}}"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{intensifier}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "love",
      "text": "love",
      "solution": "love",
      "tokens": [
        {
          "text": "love",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:love}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{stem:love}}",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "scrambled",
      "text": "{{breakfast_food}}",
      "solution": "{{breakfast_food}}",
      "tokens": [
        {
          "text": "scrambled",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "scrambled eggs",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [
            "{{breakfast_food}}"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{breakfast_food}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "eggs.",
      "text": "",
      "solution": "{{breakfast_food}}",
      "tokens": [
        {
          "text": "eggs.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "eggs",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{breakfast_food}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "scrambled eggs",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [
            "{{breakfast_food}}"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "They",
      "text": "they",
      "solution": "they",
      "tokens": [
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "they",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "are",
      "text": "are",
      "solution": "are",
      "tokens": [
        {
          "text": "are",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:be}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{stem:be}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "the",
      "text": "the",
      "solution": "the",
      "tokens": [
        {
          "text": "the",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "Shiiiiit.",
      "text": "shit",
      "solution": "shit",
      "tokens": [
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiiiiit",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [
            "shit"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{strong_bully_word}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{strong_bully_word}}",
          "topics": {
            "0": 3,
            "1": 0
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    }
  ],
  "enrichedExtended": [
    {
      "original": "I",
      "text": "i",
      "solution": "i",
      "tokens": [
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "i",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{alphabet}}",
            "ï"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{alphabet}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "ï",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "i"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{pronoun}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "I",
          "text": "i",
          "solution": "i",
          "tokens": [
            {
              "text": "I",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "I",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "i",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{alphabet}}",
                "ï"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{alphabet}}",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "ï",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "i"
              ],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{pronoun}}",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "I",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "I",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "i",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{alphabet}}",
                "ï"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            },
            {
              "text": "{{alphabet}}",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            },
            {
              "text": "ï",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "i"
              ],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            },
            {
              "text": "{{pronoun}}",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        0
      ],
      "enrichedTokens": [
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "i",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{alphabet}}",
            "ï"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        },
        {
          "text": "{{alphabet}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        },
        {
          "text": "ï",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "i"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        },
        {
          "text": "{{pronoun}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "i",
        "topics": {
          "0": 1
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [
          "{{alphabet}}",
          "ï"
        ],
        "leetMappings": [],
        "flags": [
          "COMMON"
        ],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 1,
              "name": "WHITELISTED",
              "class": "--whitelisted"
            }
          }
        ]
      }
    },
    {
      "original": "really",
      "text": "really",
      "solution": "really",
      "tokens": [
        {
          "text": "really",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{intensifier}}"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{intensifier}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "really",
          "text": "really",
          "solution": "really",
          "tokens": [
            {
              "text": "really",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{intensifier}}"
              ],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{intensifier}}",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "really",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{intensifier}}"
              ],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            },
            {
              "text": "{{intensifier}}",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        1
      ],
      "enrichedTokens": [
        {
          "text": "really",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{intensifier}}"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        },
        {
          "text": "{{intensifier}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 2,
                "name": "QUESTIONABLE",
                "class": "--questionable"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "really",
        "topics": {
          "0": 1
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [
          "{{intensifier}}"
        ],
        "leetMappings": [],
        "flags": [],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 1,
              "name": "WHITELISTED",
              "class": "--whitelisted"
            }
          }
        ]
      }
    },
    {
      "original": "love",
      "text": "love",
      "solution": "love",
      "tokens": [
        {
          "text": "love",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:love}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{stem:love}}",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "love",
          "text": "love",
          "solution": "love",
          "tokens": [
            {
              "text": "love",
              "topics": {
                "0": 2,
                "4": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{stem:love}}"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{stem:love}}",
              "topics": {
                "0": 2,
                "4": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "love",
              "topics": {
                "0": 2,
                "4": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{stem:love}}"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                },
                {
                  "riskLevel": {
                    "name": "Dating and Sexting",
                    "id": 4,
                    "class": "--dating-and-sexting"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                }
              ]
            },
            {
              "text": "{{stem:love}}",
              "topics": {
                "0": 2,
                "4": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                },
                {
                  "riskLevel": {
                    "name": "Dating and Sexting",
                    "id": 4,
                    "class": "--dating-and-sexting"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        2
      ],
      "enrichedTokens": [
        {
          "text": "love",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:love}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 2,
                "name": "QUESTIONABLE",
                "class": "--questionable"
              }
            },
            {
              "riskLevel": {
                "name": "Dating and Sexting",
                "id": 4,
                "class": "--dating-and-sexting"
              },
              "topicRate": {
                "id": 2,
                "name": "QUESTIONABLE",
                "class": "--questionable"
              }
            }
          ]
        },
        {
          "text": "{{stem:love}}",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 2,
                "name": "QUESTIONABLE",
                "class": "--questionable"
              }
            },
            {
              "riskLevel": {
                "name": "Dating and Sexting",
                "id": 4,
                "class": "--dating-and-sexting"
              },
              "topicRate": {
                "id": 2,
                "name": "QUESTIONABLE",
                "class": "--questionable"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "love",
        "topics": {
          "0": 2,
          "4": 2
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [
          "{{stem:love}}"
        ],
        "leetMappings": [],
        "flags": [
          "COMMON"
        ],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 2,
              "name": "QUESTIONABLE",
              "class": "--questionable"
            }
          },
          {
            "riskLevel": {
              "name": "Dating and Sexting",
              "id": 4,
              "class": "--dating-and-sexting"
            },
            "topicRate": {
              "id": 2,
              "name": "QUESTIONABLE",
              "class": "--questionable"
            }
          }
        ]
      }
    },
    {
      "original": "scrambled eggs.",
      "text": "",
      "solution": "{{breakfast_food}}",
      "tokens": [
        {
          "text": "eggs.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "eggs",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{breakfast_food}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "scrambled eggs",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [
            "{{breakfast_food}}"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "scrambled",
          "text": "{{breakfast_food}}",
          "solution": "{{breakfast_food}}",
          "tokens": [
            {
              "text": "scrambled",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "scrambled eggs",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [
                "{{breakfast_food}}"
              ],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{breakfast_food}}",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "scrambled",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            },
            {
              "text": "scrambled eggs",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [
                "{{breakfast_food}}"
              ],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                }
              ]
            },
            {
              "text": "{{breakfast_food}}",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                }
              ]
            }
          ]
        },
        {
          "original": "eggs.",
          "text": "",
          "solution": "{{breakfast_food}}",
          "tokens": [
            {
              "text": "eggs.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "eggs",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{breakfast_food}}",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "scrambled eggs",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [
                "{{breakfast_food}}"
              ],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "eggs.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "eggs",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            },
            {
              "text": "{{breakfast_food}}",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                }
              ]
            },
            {
              "text": "scrambled eggs",
              "topics": {
                "0": 2
              },
              "language": "en",
              "altSpellings": [
                "{{breakfast_food}}"
              ],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 2,
                    "name": "QUESTIONABLE",
                    "class": "--questionable"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        3,
        4
      ],
      "enrichedTokens": [
        {
          "text": "eggs.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "eggs",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        },
        {
          "text": "{{breakfast_food}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 2,
                "name": "QUESTIONABLE",
                "class": "--questionable"
              }
            }
          ]
        },
        {
          "text": "scrambled eggs",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [
            "{{breakfast_food}}"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 2,
                "name": "QUESTIONABLE",
                "class": "--questionable"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "{{breakfast_food}}",
        "topics": {
          "0": 2
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [],
        "leetMappings": [],
        "flags": [],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 2,
              "name": "QUESTIONABLE",
              "class": "--questionable"
            }
          }
        ]
      }
    },
    {
      "original": "They",
      "text": "they",
      "solution": "they",
      "tokens": [
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "they",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "They",
          "text": "they",
          "solution": "they",
          "tokens": [
            {
              "text": "They",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "They",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "they",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "They",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "They",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "they",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        5
      ],
      "enrichedTokens": [
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "they",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "they",
        "topics": {
          "0": 1
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [],
        "leetMappings": [],
        "flags": [],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 1,
              "name": "WHITELISTED",
              "class": "--whitelisted"
            }
          }
        ]
      }
    },
    {
      "original": "are",
      "text": "are",
      "solution": "are",
      "tokens": [
        {
          "text": "are",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:be}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{stem:be}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "are",
          "text": "are",
          "solution": "are",
          "tokens": [
            {
              "text": "are",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{stem:be}}"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{stem:be}}",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "are",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{stem:be}}"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            },
            {
              "text": "{{stem:be}}",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        6
      ],
      "enrichedTokens": [
        {
          "text": "are",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:be}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        },
        {
          "text": "{{stem:be}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "are",
        "topics": {
          "0": 1
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [
          "{{stem:be}}"
        ],
        "leetMappings": [],
        "flags": [
          "COMMON"
        ],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 1,
              "name": "WHITELISTED",
              "class": "--whitelisted"
            }
          }
        ]
      }
    },
    {
      "original": "the",
      "text": "the",
      "solution": "the",
      "tokens": [
        {
          "text": "the",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "the",
          "text": "the",
          "solution": "the",
          "tokens": [
            {
              "text": "the",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "the",
              "topics": {
                "0": 1
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 1,
                    "name": "WHITELISTED",
                    "class": "--whitelisted"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        7
      ],
      "enrichedTokens": [
        {
          "text": "the",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 1,
                "name": "WHITELISTED",
                "class": "--whitelisted"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "the",
        "topics": {
          "0": 1
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [],
        "leetMappings": [],
        "flags": [
          "COMMON"
        ],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 1,
              "name": "WHITELISTED",
              "class": "--whitelisted"
            }
          }
        ]
      }
    },
    {
      "original": "Shiiiiit.",
      "text": "shit",
      "solution": "shit",
      "tokens": [
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiiiiit",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [
            "shit"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{strong_bully_word}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{strong_bully_word}}",
          "topics": {
            "0": 3,
            "1": 0
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ],
      "grouped": [
        {
          "original": "Shiiiiit.",
          "text": "shit",
          "solution": "shit",
          "tokens": [
            {
              "text": "Shiiiiit.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "Shiiiiit.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "shiiiiit.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "shiiiiit",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "shiit",
              "topics": {
                "0": 5,
                "5": 5
              },
              "language": "en",
              "altSpellings": [
                "shit"
              ],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "shit",
              "topics": {
                "0": 5,
                "5": 5
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{strong_bully_word}}"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            },
            {
              "text": "{{strong_bully_word}}",
              "topics": {
                "0": 3,
                "1": 0
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0
            }
          ],
          "grouped": [],
          "indices": [],
          "enrichedTokens": [
            {
              "text": "Shiiiiit.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "Shiiiiit.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "shiiiiit.",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "shiiiiit",
              "topics": {
                "0": 4
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 4,
                    "name": "MILD",
                    "class": "--mild"
                  }
                }
              ]
            },
            {
              "text": "shiit",
              "topics": {
                "0": 5,
                "5": 5
              },
              "language": "en",
              "altSpellings": [
                "shit"
              ],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 5,
                    "name": "OFFENSIVE",
                    "class": "--offensive"
                  }
                },
                {
                  "riskLevel": {
                    "name": "Vulgar",
                    "id": 5,
                    "class": "--vulgar"
                  },
                  "topicRate": {
                    "id": 5,
                    "name": "OFFENSIVE",
                    "class": "--offensive"
                  }
                }
              ]
            },
            {
              "text": "shit",
              "topics": {
                "0": 5,
                "5": 5
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [
                "{{strong_bully_word}}"
              ],
              "leetMappings": [],
              "flags": [
                "COMMON"
              ],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 5,
                    "name": "OFFENSIVE",
                    "class": "--offensive"
                  }
                },
                {
                  "riskLevel": {
                    "name": "Vulgar",
                    "id": 5,
                    "class": "--vulgar"
                  },
                  "topicRate": {
                    "id": 5,
                    "name": "OFFENSIVE",
                    "class": "--offensive"
                  }
                }
              ]
            },
            {
              "text": "{{strong_bully_word}}",
              "topics": {
                "0": 3,
                "1": 0
              },
              "language": "en",
              "altSpellings": [],
              "altSenses": [],
              "leetMappings": [],
              "flags": [],
              "taskIds": [],
              "dateCreated": 0,
              "dateUpdated": 0,
              "clientId": 0,
              "enrichedTopics": [
                {
                  "riskLevel": {
                    "name": "General Risk",
                    "id": 0,
                    "class": "--general"
                  },
                  "topicRate": {
                    "id": 3,
                    "name": "UNKNOWN",
                    "class": "--unknown"
                  }
                },
                {
                  "riskLevel": {
                    "name": "Bullying",
                    "id": 1,
                    "class": "--bullying"
                  },
                  "topicRate": {
                    "id": 0,
                    "name": "SUPERSAFE",
                    "class": "--supersafe"
                  }
                }
              ]
            }
          ]
        }
      ],
      "indices": [
        8
      ],
      "enrichedTokens": [
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "shiiiiit",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 4,
                "name": "MILD",
                "class": "--mild"
              }
            }
          ]
        },
        {
          "text": "shiit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [
            "shit"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 5,
                "name": "OFFENSIVE",
                "class": "--offensive"
              }
            },
            {
              "riskLevel": {
                "name": "Vulgar",
                "id": 5,
                "class": "--vulgar"
              },
              "topicRate": {
                "id": 5,
                "name": "OFFENSIVE",
                "class": "--offensive"
              }
            }
          ]
        },
        {
          "text": "shit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{strong_bully_word}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 5,
                "name": "OFFENSIVE",
                "class": "--offensive"
              }
            },
            {
              "riskLevel": {
                "name": "Vulgar",
                "id": 5,
                "class": "--vulgar"
              },
              "topicRate": {
                "id": 5,
                "name": "OFFENSIVE",
                "class": "--offensive"
              }
            }
          ]
        },
        {
          "text": "{{strong_bully_word}}",
          "topics": {
            "0": 3,
            "1": 0
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0,
          "enrichedTopics": [
            {
              "riskLevel": {
                "name": "General Risk",
                "id": 0,
                "class": "--general"
              },
              "topicRate": {
                "id": 3,
                "name": "UNKNOWN",
                "class": "--unknown"
              }
            },
            {
              "riskLevel": {
                "name": "Bullying",
                "id": 1,
                "class": "--bullying"
              },
              "topicRate": {
                "id": 0,
                "name": "SUPERSAFE",
                "class": "--supersafe"
              }
            }
          ]
        }
      ],
      "solvedToken": {
        "text": "shit",
        "topics": {
          "0": 5,
          "5": 5
        },
        "language": "en",
        "altSpellings": [],
        "altSenses": [
          "{{strong_bully_word}}"
        ],
        "leetMappings": [],
        "flags": [
          "COMMON"
        ],
        "taskIds": [],
        "dateCreated": 0,
        "dateUpdated": 0,
        "clientId": 0,
        "enrichedTopics": [
          {
            "riskLevel": {
              "name": "General Risk",
              "id": 0,
              "class": "--general"
            },
            "topicRate": {
              "id": 5,
              "name": "OFFENSIVE",
              "class": "--offensive"
            }
          },
          {
            "riskLevel": {
              "name": "Vulgar",
              "id": 5,
              "class": "--vulgar"
            },
            "topicRate": {
              "id": 5,
              "name": "OFFENSIVE",
              "class": "--offensive"
            }
          }
        ]
      }
    }
  ],
  "enrichedTopics": [
    {
      "riskLevel": {
        "name": "General Risk",
        "id": 0,
        "class": "--general"
      },
      "topicRate": {
        "id": 5,
        "name": "OFFENSIVE",
        "class": "--offensive"
      }
    },
    {
      "riskLevel": {
        "name": "Dating and Sexting",
        "id": 4,
        "class": "--dating-and-sexting"
      },
      "topicRate": {
        "id": 2,
        "name": "QUESTIONABLE",
        "class": "--questionable"
      }
    },
    {
      "riskLevel": {
        "name": "Vulgar",
        "id": 5,
        "class": "--vulgar"
      },
      "topicRate": {
        "id": 5,
        "name": "OFFENSIVE",
        "class": "--offensive"
      }
    }
  ],
  "enrichedPredictions": [
    {
      "key": "lang_en",
      "name": "en",
      "value": 98
    },
    {
      "key": "lang_fr",
      "name": "fr",
      "value": 2
    },
    {
      "key": "topic_fraud",
      "name": "fraud",
      "value": 10
    }
  ],
  "globalChat": {
    "name": "Global chat",
    "class": "--global-chat",
    "icon": "thumbs-up"
  },
  "privateChat": null
}
  `);
}
