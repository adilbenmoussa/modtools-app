import {Component, Input, EventEmitter, Output, OnInit} from '@angular/core';
import {sortBy} from 'lodash';
import {UserService} from '../../shared-components/user.service';
import {Client, Clients} from '../../../constants';

@Component({
  selector: 'app-client-picker',
  templateUrl: './client-picker.component.html',
  styleUrls: ['./client-picker.component.less']
})
export class ClientPickerComponent implements OnInit {

  @Input() selectedClient = 60;
  @Output() clientChanged = new EventEmitter<number>();

  clients: Client[] = [];

  constructor(private userService: UserService) {
  }

  async ngOnInit() {
    // Get the current user
    const user = await this.userService.me();

    // Filter the user's languages by the ones they're allowed to use
    if (user?.config?.allowedClients) {
      this.clients = Clients.filter(lang => user.config.allowedClients.includes(lang.id));
    }

    // Sort by language name, not code.
    this.clients = sortBy(this.clients, 'name');
  }

  onClientChanged() {
    this.clientChanged.emit(this.selectedClient);
  }

}
