import {TextRiskLevelClassification, TextRiskLevelClassificationEnum} from '../../constants';
import {IconName} from '@fortawesome/fontawesome-svg-core'

export type ContentTypeEnum = 'SHORT_TEXT' | 'LONG_TEXT' | 'USERNAME';
export type FlagEnum =
  'COMMON'
  | 'WATCH'
  | 'SKIP'
  | 'PHRASE_RULE'
  | 'USERNAME'
  | 'FIRST_NAME'
  | 'LAST_NAME'
  | 'CITY'
  | 'STATE'
  | 'COUNTRY'
  | 'EMAIL_ADDRESS';

export type LanguageEnum =
  'ar'
  | 'da'
  | 'de'
  | 'en'
  | 'es'
  | 'fi'
  | 'fr'
  | 'hi'
  | 'id'
  | 'it'
  | 'ja'
  | 'ko'
  | 'nl'
  | 'no'
  | 'pl'
  | 'pt'
  | 'ro'
  | 'ru'
  | 'sv'
  | 'th'
  | 'tr'
  | 'vi'
  | 'zh';

export type TopicRateEnum = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;

export interface Topics {
  description?: string;
  0?: TopicRateEnum;
  1?: TopicRateEnum;
  2?: TopicRateEnum;
  3?: TopicRateEnum;
  4?: TopicRateEnum;
  5?: TopicRateEnum;
  6?: TopicRateEnum;
  7?: TopicRateEnum;
  8?: TopicRateEnum;
  9?: TopicRateEnum;
  10?: TopicRateEnum;
  11?: TopicRateEnum;
  12?: TopicRateEnum;
  13?: TopicRateEnum;
  14?: TopicRateEnum;
  15?: TopicRateEnum;
  16?: TopicRateEnum;
  17?: TopicRateEnum;
  18?: TopicRateEnum;
  19?: TopicRateEnum;
  20?: TopicRateEnum;
  21?: TopicRateEnum;
  22?: TopicRateEnum;
  23?: TopicRateEnum;
  24?: TopicRateEnum;
  25?: TopicRateEnum;
  26?: TopicRateEnum;
  27?: TopicRateEnum;
  28?: TopicRateEnum;
  29?: TopicRateEnum;
  30?: TopicRateEnum;
  31?: TopicRateEnum;
}

export interface TestRule {
  text?: string;
  topics: Topics;
}

export interface TextInput {
  clientId: number;
  contentType?: ContentTypeEnum;
  language: LanguageEnum;
  text: string;
  pre?: string;
  config?: ConfigTextClassifier;
  testRules?: Array<TestRule>;
}

export interface ConfigLanguage {
  languages?: Array<LanguageEnum>;
  trumpLanguage?: number;
}

export interface ConfigTextClassifier {
  inheritanceChain?: Array<number>;
  language?: ConfigLanguage;
}

export interface Token {
  text?: string;
  topics?: Topics;
  language: LanguageEnum;
  altSpellings: Array<string>;
  altSenses: Array<string>;
  leetMappings: Array<string>;
  flags: Array<FlagEnum>;
  taskIds?: Array<string>;
  dateCreated: number;
  dateUpdated: number;
  clientId: number;
}

export interface EnrichedToken extends Token {
  enrichedTopics?: EnrichedTopics[];
  maxEnrichedTopic?: EnrichedTopics;
}

export interface Slots {
  original?: string;
  text?: string;
  solution?: string;
  tokens?: Array<Token>;
  startPos?: number;
}

export interface EnrichedSlots extends Slots {
  enrichedTokens?: Array<EnrichedToken>;
  solvedToken?: EnrichedToken;
  grouped?: EnrichedSlots[];
  language?: string;
}

export interface FailingFragment {
  text?: string;
  normalized?: string;
  redactedText?: string;
  topics?: Topics;
  startPos?: number;
  endPos?: number;
}

export interface Predictions {
  [key: string]: any;
}

export interface EnrichedPrediction {
  key: string;
  name: string;
  value: number;
}

export interface TextClassifiedOutput {
  extended?: Array<Slots>;
  failingFragments?: Array<FailingFragment>;
  language?: LanguageEnum;
  pre?: string;
  predictions?: Predictions;
  pseudonmized?: boolean;
  recentRules?: Array<string>;
  redactedText?: string;
  simplified?: string;
  text?: string;
  topics?: Topics;
}

export interface EnrichedTopicRateEnum {
  name: string;
  class: string;
  id: number;
}

export interface EnrichedTopics {
  riskLevel: TextRiskLevelClassification;
  topicRate: EnrichedTopicRateEnum;
}

export interface GroupedEnrichedSlots {
  [key: string]: EnrichedSlots;
}

export enum PolicyGuide {
  GlobalChat = 'Global chat',
  PrivateChat = 'Private chat',
}

export interface PolicyGuideCondition {
  riskLevelId: number;
  topicRateId: number;
}

export interface ChatView {
  name: string;
  class: string;
  icon: IconName;
}

export interface EnrichedTextClassifiedOutput extends TextClassifiedOutput {
  enrichedExtended?: Array<EnrichedSlots>;
  enrichedTopics?: EnrichedTopics[];
  enrichedPredictions?: EnrichedPrediction[];
}
