import {Component, Input, OnInit} from '@angular/core';
import {EnrichedPrediction} from '../../../filter-quality-interfaces';

@Component({
  selector: 'app-ai-prediction-view',
  templateUrl: './ai-prediction-view.component.html',
  styleUrls: ['./ai-prediction-view.component.less']
})
export class AiPredictionViewComponent implements OnInit {
  @Input() enrichedPrediction: EnrichedPrediction;

  ngOnInit() {
  }
}
