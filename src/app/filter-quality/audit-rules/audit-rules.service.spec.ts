import {TestBed} from '@angular/core/testing';
import {AuditRulesService} from './audit-rules.service';
import {cold} from 'jasmine-marbles';
import {TextInput} from '../filter-quality-interfaces';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {enrichedTextClassifiedOutputMock} from '../../mocks/enriched-text-classified-output.mock';
import {textClassifiedOutputMock} from '../../mocks/text-classified-output.mock';
import {HttpClient} from '@angular/common/http';
import {AuditRulesUtilNamespace as auditRulesUtil} from '../../utils/audit-rules-util';
import {HttpUtilNamespace as httpUtil} from '../../utils/http-utils';

describe('AuditRulesService', () => {
  let service: AuditRulesService;
  let httpClient: HttpClient;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuditRulesService,
        {
          provide: HttpClient,
          useValue: {
            post: () => {
            }
          }
        }
      ],
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(AuditRulesService);
    httpClient = TestBed.inject(HttpClient);
  });

  describe('classifyText', () => {
    const body: TextInput = {
      clientId: 1,
      language: 'nl',
      text: 'some text'
    };

    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should return an Observable<EnrichedTextClassifiedOutput>', () => {
      // Arrange
      const rawResponse = textClassifiedOutputMock();
      const parsedResponse = enrichedTextClassifiedOutputMock();
      const rawResponse$ = cold('-a|', {a: rawResponse}); // generates a observable result object

      spyOn(auditRulesUtil, 'enrichTextClassifiedOutput').and.returnValue(parsedResponse);
      spyOn(httpClient, 'post').and.returnValue(rawResponse$);

      // Act
      const result$ = service.classifyText(body);

      // Assert
      result$.subscribe(result => {
        expect(auditRulesUtil.enrichTextClassifiedOutput).toHaveBeenCalledWith(rawResponse);
        expect(result).toEqual(parsedResponse);
      });
    });

    it('should throw an error when the request fails', () => {
      // Arrange
      const response: any = {
        code: '0102',
        displayMessage: 'an error message'
      };
      const response$ = cold('-#', {}, response); // generates a observable error object

      spyOn(httpClient, 'post').and.returnValue(response$);

      // Act
      const result$ = service.classifyText(body);

      // Assert
      result$.subscribe(() => {
        expect(httpUtil.handleError).toHaveBeenCalledWith(response);
      });
    });
  });
});
