import {Observable, of as observableOf} from 'rxjs';
import * as Sentry from '@sentry/browser';

/**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */

export function handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // For example send some errors to Sentry...
    Sentry.addBreadcrumb({
      category: 'some_category',
      message: `${operation} failed: ${error.message}`,
      level: Sentry.Severity.Error
    });
    Sentry.captureException(error);

    // TODO: better job of transforming error for user consumption
    console.error(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return observableOf(result as T);
  };
}

export function HttpUtilNamespace() {
}
HttpUtilNamespace.handleError = handleError;
