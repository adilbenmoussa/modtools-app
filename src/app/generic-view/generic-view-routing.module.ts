import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {GenericViewComponent} from './generic-view.component';

const routes: Routes = [{
  path: '',
  component: GenericViewComponent,
  children: [
    {
      path: 'filter-quality',
      loadChildren: () => import('../filter-quality/filter-quality.module').then(m => m.FilterQualityModule),
      data: {breadcrumb: 'Filter Quality', title: 'Filter Quality'}
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenericViewRoutingModule {
}
