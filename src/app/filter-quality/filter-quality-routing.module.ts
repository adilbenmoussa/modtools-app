import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FilterQualityComponent} from './filter-quality.component';
import {AuditRulesComponent} from './audit-rules/audit-rules.component';


const routes: Routes = [{
  path: '',
  component: FilterQualityComponent,
  children: [
    {
      path: 'audit-rules',
      component: AuditRulesComponent,
      data: {breadcrumb: 'Audit Rules', title: 'Audit Rules'},
      children: [
        {
          path: 'diagnose',
          component: AuditRulesComponent,
          data: {breadcrumb: 'Diagnose', title: 'Diagnose'},
        }
      ]
    },
    {path: '', redirectTo: 'audit-rules', pathMatch: 'full'},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilterQualityRoutingModule {
}
