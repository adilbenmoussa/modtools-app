import {Component, OnDestroy, OnInit} from '@angular/core';
import {SearchService} from '../main/search/search.service';
import {filter} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {Unsubscribable} from '../decorators/unsubscribable.decorator';
import {lookUpPrimaryOutletData} from '../utils/route.utils';

@Component({
  selector: 'app-generic-view',
  templateUrl: './generic-view.component.html',
  styleUrls: ['./generic-view.component.less']
})
@Unsubscribable()
export class GenericViewComponent implements OnInit, OnDestroy {
  title = '';

  constructor(private searchService: SearchService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.searchService
      .refreshClickedOrSearched$
      .pipe(
        filter(text => !!text)
      )
      .subscribe(async () => {
        await this.router.navigate(['/filter-quality/audit-rules/diagnose'])
      })
      .unsubscribeOnDestroy(this);

    lookUpPrimaryOutletData(this.router.events, this.activatedRoute)
      .subscribe((route) => this.title = route.title)
      .unsubscribeOnDestroy(this);
  }

  ngOnDestroy(): void {
  }
}
