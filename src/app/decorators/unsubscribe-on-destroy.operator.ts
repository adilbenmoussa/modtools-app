/**
 * Rxjs helper operator for the @Unsubscribable decorator
 * @author: Adil Ben Moussa <https://github.com/adilbenmoussa>
 * @license: MIT
 * @see: @Unsubscribable decorator
 **/

export function unsubscribeOnDestroy(context: any) {
  if (context) {
    context.disposeBag = context.disposeBag || [];
    context.disposeBag.push(this);
  } else {
    console.error('Subscription is using unsubscribeOnDestroy but does not pass the correct context');
  }
}
