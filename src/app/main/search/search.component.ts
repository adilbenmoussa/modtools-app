import {Component, OnInit} from '@angular/core';
import {SearchService} from './search.service';

@Component({
  selector: 'app-main-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {

  constructor(public searchService: SearchService) {
    // searchService.searchText$.subscribe(v => console.log('SearchComponent', v));
  }

  ngOnInit() {
  }
}
