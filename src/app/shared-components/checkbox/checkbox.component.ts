import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.less']
})
export class CheckboxComponent {
  @Input() selected: boolean;
  @Input() text: string;
  @Output() selectionChanged = new EventEmitter<any>();

  onToggle() {
    this.selected = !this.selected;
    this.selectionChanged.emit(this.selected);
    console.log('this.selected', this.selected);
  }
}
