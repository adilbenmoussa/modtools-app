import {OnDestroy, OnInit} from '@angular/core';
import {of as observableOf} from 'rxjs';

import {Unsubscribable} from './unsubscribable.decorator';
import {unsubscribeOnDestroy} from './unsubscribe-on-destroy.operator';

const {Subscription} = require('rxjs');
Subscription.prototype.unsubscribeOnDestroy = unsubscribeOnDestroy;

describe('Unsubscribable', () => {

  it('should throw an error when OnDestroy is not implemented', () => {
    const errorMessage = 'TestComponent is using @Unsubscribable() but does not implement ngOnDestroy, this wont work on AOT mode!';
    spyOn(console, 'error');

    @Unsubscribable()
    class TestComponent {
    }

    expect(console.error).toHaveBeenCalledWith(errorMessage);
  });

  describe('ngOnInit', () => {
    it('should add the disposeBag list if it doesn\'t exist already', () => {

      @Unsubscribable()
      class TestComponent implements OnInit, OnDestroy {
        disposeBag;
        ngOnInit() {
        }

        ngOnDestroy() {
        }
      }

      const comp = new TestComponent();

      comp.ngOnInit();

      expect(comp.disposeBag.length).toEqual(0);
    });

    it('should call the original OnInit after adding the disposeBag list', () => {
      spyOn(console, 'log');

      @Unsubscribable()
      class TestComponent implements OnInit, OnDestroy {
        ngOnInit() {
          console.log('Original ngOnInit');
        }

        ngOnDestroy() {
        }
      }

      const comp = new TestComponent();

      comp.ngOnInit();

      expect(console.log).toHaveBeenCalledWith('Original ngOnInit');
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe all items from disposeBag list', () => {

      @Unsubscribable()
      class TestComponent implements OnInit, OnDestroy {
        subscription1;
        subscription2;

        ngOnInit() {
          this.subscription1 =
            observableOf('fristItem')
              .subscribe(() => {
              });
          this.subscription2 =
            observableOf('secondItem')
              .subscribe(() => {
              });

          spyOn(this.subscription1, 'unsubscribe').and.callThrough();
          spyOn(this.subscription2, 'unsubscribe').and.callThrough();

          this.subscription1
            .unsubscribeOnDestroy(this);

          this.subscription2
            .unsubscribeOnDestroy(this);
        }

        ngOnDestroy() {
        }
      }

      const comp = new TestComponent();
      comp.ngOnInit();
      comp.ngOnDestroy();

      expect(comp.subscription1.unsubscribe).toHaveBeenCalled();
      expect(comp.subscription2.unsubscribe).toHaveBeenCalled();
    });

    it('should delete-action disposeBag list after unsubscribing all subscriptions from disposeBag list', () => {

      @Unsubscribable()
      class TestComponent implements OnInit, OnDestroy {
        disposeBag;

        ngOnInit() {
        }

        ngOnDestroy() {
        }
      }

      const comp = new TestComponent();
      comp.ngOnInit();
      comp.ngOnDestroy();

      expect(comp.disposeBag).toBeUndefined();
    });

    it('should call the original OnDestroy after deleting the disposeBag list', () => {
      spyOn(console, 'log');

      @Unsubscribable()
      class TestComponent implements OnInit, OnDestroy {
        ngOnInit() {
        }

        ngOnDestroy() {
          console.log('Original ngOnDestroy');
        }
      }

      const comp = new TestComponent();
      comp.ngOnDestroy();

      expect(console.log).toHaveBeenCalledWith('Original ngOnDestroy');
    });
  });
});


