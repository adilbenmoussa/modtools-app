import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {lookUpPrimaryOutletData} from '../../utils/route.utils';
import {Unsubscribable} from '../../decorators/unsubscribable.decorator';

interface IBreadcrumb {
  label: string;
  params: Params;
  url: string;
}

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.less']
})
@Unsubscribable()
export class BreadcrumbsComponent implements OnInit, OnDestroy {

  public breadcrumbs: IBreadcrumb[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.breadcrumbs = [];
  }

  ngOnInit() {
    lookUpPrimaryOutletData(this.router.events, this.activatedRoute)
      .subscribe(route => {
        const snapshot = this.router.routerState.snapshot;
        this.breadcrumbs = [];
        const url = snapshot.url;
        const routeData = route.snapshot.data;

        const label = routeData.breadcrumb;
        const params = snapshot.root.params;

        const crumb: IBreadcrumb = {
          url,
          label,
          params
        };
        this.breadcrumbs.push(crumb);
      })
      .unsubscribeOnDestroy(this);
  }

  ngOnDestroy(): void {
  }
}
