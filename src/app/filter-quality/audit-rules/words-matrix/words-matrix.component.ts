import { Component, Input } from '@angular/core';
import {EnrichedSlots} from '../../filter-quality-interfaces';

@Component({
  selector: 'app-words-matrix',
  templateUrl: './words-matrix.component.html',
  styleUrls: ['./words-matrix.component.less']
})
export class WordsMatrixComponent {

  @Input() wordsMatrix:Array<EnrichedSlots>;

  riskLevelClass(words: EnrichedSlots): string {
    return words.solvedToken?.enrichedTopics[0]?.topicRate?.class;
  }
}
