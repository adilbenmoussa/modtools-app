import {
  ChatView, EnrichedPrediction,
  EnrichedSlots,
  EnrichedTextClassifiedOutput,
  EnrichedToken,
  EnrichedTopics,
  GroupedEnrichedSlots, PolicyGuide, PolicyGuideCondition,
  Slots,
  TextClassifiedOutput,
  Token, TopicRateEnum, Topics
} from '../filter-quality/filter-quality-interfaces';
import {TextRiskLevelClassifications, TopicsEnum} from '../../constants';
import {findIndex, last, maxBy, sortBy, uniqBy, some} from 'lodash';

const initMaxEnrichedTopic = (enrichedTopics) => maxBy(enrichedTopics, (topic: EnrichedTopics) => topic.topicRate.id);

function initExtendTopicsObj(topics: Topics): EnrichedTopics[] {
  return Object
    .keys(topics)
    .map((key: TopicRateEnum | string) => ({
      riskLevel: TopicsEnum[key],
      topicRate: TextRiskLevelClassifications[topics[key]]
    }));
}

function initExtendTokensObj(token: Token): EnrichedToken {
  const enrichedTopics = initExtendTopicsObj(token.topics);
  const maxEnrichedTopic = initMaxEnrichedTopic(enrichedTopics);
  return {
    ...token,
    enrichedTopics,
    maxEnrichedTopic
  };
}

function initExtendSlotObj(slots: Slots): EnrichedSlots {
  return {
    ...slots,
    grouped: [],
    enrichedTokens: slots
      .tokens
      .map(initExtendTokensObj)
  };
}

function groupDuplicateSolution(accumulator: GroupedEnrichedSlots, currentValue: EnrichedSlots): GroupedEnrichedSlots {
  const grouped = accumulator[currentValue.solution]?.grouped || [];
  grouped.push(currentValue);
  let enrichedSlots = currentValue;
  if (grouped.length > 1) {
    let tokens = [];
    let enrichedTokens = [];
    grouped.forEach(group => {
      tokens = tokens.concat(group.tokens);
      enrichedTokens = enrichedTokens.concat(group.enrichedTokens);
    });
    // remove the duplicates
    tokens = uniqBy(tokens, token => token.text);
    enrichedTokens = uniqBy(enrichedTokens, enrichedToken => enrichedToken.text);
    enrichedSlots = {
      ...currentValue,
      tokens,
      enrichedTokens
    }
  }
  accumulator[currentValue.solution] = {
    ...enrichedSlots,
    grouped
  };

  return accumulator;
}

function mergeOriginalTextForGroupedSlots(enrichedSlots: EnrichedSlots): EnrichedSlots {
  return {
    ...enrichedSlots,
    original: enrichedSlots.grouped?.length > 1 ?
      enrichedSlots.grouped.map(({original}) => original).join(' ') :
      enrichedSlots.original
  };
}

function initSolvedAndSortTokenObj(enrichedSlots: EnrichedSlots): EnrichedSlots {
  const solvedToken = enrichedSlots
    .enrichedTokens
    .filter((token: EnrichedToken) => token.text === enrichedSlots.solution)
    .pop();

  // move the solved token to the front
  const enrichedTokens = sortBy(enrichedSlots.enrichedTokens, (token: EnrichedToken) => token === solvedToken ? 0 : 1);

  return {
    ...enrichedSlots,
    enrichedTokens,
    solvedToken
  };
}

export function getChatViewObj(enrichedSlots: EnrichedTopics[]) {
  return function (type: PolicyGuide, policyGuideConditions: PolicyGuideCondition[]): ChatView | null {
    const found = some(policyGuideConditions, condition => {
      return findIndex(enrichedSlots, enrichedSlot => {
        return condition.riskLevelId === enrichedSlot.riskLevel.id &&
          condition.topicRateId >= enrichedSlot.topicRate.id
      }) > -1;
    });

    if (found && type === PolicyGuide.GlobalChat) {
      return {
        name: 'Global chat',
        class: '--global-chat',
        icon: 'thumbs-up'
      };
    }

    if (found && type === PolicyGuide.PrivateChat) {
      return {
        name: 'Private Chat',
        class: '--private-chat',
        icon: 'thumbs-down'
      };
    }
    return null;
  }
}

function enrichTextClassifiedOutput(input: TextClassifiedOutput): EnrichedTextClassifiedOutput {
  if (!input) return;

  let enrichedExtended: EnrichedSlots[];
  let enrichedTopics: EnrichedTopics[];
  let enrichedPredictions: EnrichedPrediction[];
  if (input.topics) {
    enrichedTopics = initExtendTopicsObj(input.topics);
  }

  if (input.extended) {
    const groupedEnrichedSlots: GroupedEnrichedSlots = input
      .extended
      .map(initExtendSlotObj)
      .reduce<GroupedEnrichedSlots>(groupDuplicateSolution, {});

    enrichedExtended = Object
      .values(groupedEnrichedSlots)
      .map<EnrichedSlots>(mergeOriginalTextForGroupedSlots)
      .map<EnrichedSlots>(initSolvedAndSortTokenObj);
  }

  if (input.predictions) {
    enrichedPredictions = Object
      .keys(input.predictions)
      .map((key: string) => {
        const keyParts = key.split('_');
        return {
          key,
          name: keyParts.length > 1 ? last(keyParts) : '',
          value: Math.round(input.predictions[key] * 100)
        }
      });
  }

  return {
    ...input,
    enrichedExtended,
    enrichedTopics,
    enrichedPredictions,
  };
}

export function AuditRulesUtilNamespace() {
}

AuditRulesUtilNamespace.enrichTextClassifiedOutput = enrichTextClassifiedOutput;
