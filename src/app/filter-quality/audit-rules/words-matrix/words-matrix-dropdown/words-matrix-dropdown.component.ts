import {Component, Input} from '@angular/core';
import {EnrichedSlots} from '../../../filter-quality-interfaces';
import {faComments} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-words-matrix-dropdown',
  templateUrl: './words-matrix-dropdown.component.html',
  styleUrls: ['./words-matrix-dropdown.component.less']
})
export class WordsMatrixDropdownComponent {
  @Input() words: EnrichedSlots;
  faComments = faComments;
}
