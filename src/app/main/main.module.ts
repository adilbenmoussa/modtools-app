import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';

import {LogoComponent} from './logo/logo.component';
import {HeaderComponent} from './header/header.component';
import {SearchComponent} from './search/search.component';
import {A11yFlyoutComponent} from './a11y-flyout/a11y-flyout.component';
import {UserFlyoutComponent} from './user-flyout/user-flyout.component';
import {SearchService} from './search/search.service';
import {SubHeaderComponent} from './sub-header/sub-header.component';

@NgModule({
  declarations: [
    LogoComponent,
    HeaderComponent,
    SubHeaderComponent,
    SearchComponent,
    A11yFlyoutComponent,
    UserFlyoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    HeaderComponent
  ],
  providers: [
    SearchService
  ]
})
export class MainModule {
}
