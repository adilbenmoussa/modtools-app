import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {sortBy} from 'lodash';

import {ContentTypes} from 'src/constants';
import {ContentTypeEnum} from '../../filter-quality/filter-quality-interfaces';

@Component({
  selector: 'app-content-type-picker',
  templateUrl: './content-type-picker.component.html',
  styleUrls: ['./content-type-picker.component.less']
})
export class ContentTypePickerComponent implements OnInit {

  @Input() selectedContentType: ContentTypeEnum = ContentTypes[0].id;
  @Output() contentTypeChanged = new EventEmitter<ContentTypeEnum>();

  contentTypes = ContentTypes;

  constructor() {
  }

  ngOnInit() {
    this.contentTypes = sortBy(ContentTypes, 'name');
  }

  onContentTypeChanged() {
    this.contentTypeChanged.emit(this.selectedContentType);
  }
}
