import {Component, OnInit} from '@angular/core';

import {UserService} from 'src/app/shared-components/user.service';
import {User} from 'src/app/shared-components/user';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.less']
})
export class SubHeaderComponent {

  constructor(public userService: UserService) {
  }
}
