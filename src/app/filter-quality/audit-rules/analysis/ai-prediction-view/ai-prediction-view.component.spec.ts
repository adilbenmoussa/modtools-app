import {ComponentFixture, TestBed, async, ComponentFixtureAutoDetect} from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AiPredictionViewComponent} from './ai-prediction-view.component';
import {enrichedTextClassifiedOutputMock} from '../../../../mocks/enriched-text-classified-output.mock';

describe('AiPredictionViewComponent', () => {
  let component: AiPredictionViewComponent;
  let fixture: ComponentFixture<AiPredictionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [AiPredictionViewComponent],
      providers: [
        {provide: ComponentFixtureAutoDetect, useValue: true}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const enrichedTextClassifiedOutput = enrichedTextClassifiedOutputMock();
    fixture = TestBed.createComponent(AiPredictionViewComponent);
    component = fixture.componentInstance;
    component.enrichedPrediction = enrichedTextClassifiedOutput.enrichedPredictions[0];
    fixture.detectChanges();
  });

  it('should create', async(() => {
    expect(component).toBeDefined();
  }));

  it('should display the prediction name', async(() => {
    const nameElem: HTMLElement = fixture.nativeElement.querySelector('.container__name');
    expect(nameElem.textContent).toContain('en');
  }));

  it('should set the currect prediction value on the progress', async(() => {
    const progressElem: HTMLInputElement = fixture.nativeElement.querySelector('.container__progress');
    expect(progressElem.value.toString()).toEqual('98');
  }));

  it('should display the prediction value', async(() => {
    const valueElem: HTMLElement = fixture.nativeElement.querySelector('.container__value');
    expect(valueElem.textContent).toContain('98%');
  }));
});
