import {TextClassifiedOutput} from '../filter-quality/filter-quality-interfaces';

export function textClassifiedOutputMock(): TextClassifiedOutput {
  return JSON.parse(`
  {
  "failingFragments": [
    {
      "endPos": 52,
      "normalized": "shit",
      "redactedText": "",
      "startPos": 44,
      "text": "shit",
      "topics": {
        "0": 5,
        "5": 5
      }
    }
  ],
  "language": "en",
  "predictions": {
    "lang_en": 0.9800100136,
    "lang_fr": 0.0190100136,
    "topic_fraud": 0.10001
  },
  "pseudonmized": false,
  "simplified": "i really love {{breakfast_food}} they are the shit",
  "text": "I really love scrambled eggs.  They are the Shiiiiit.",
  "topics": {
    "0": 5,
    "4": 2,
    "5": 5
  },
  "extended": [
    {
      "original": "I",
      "text": "i",
      "solution": "i",
      "tokens": [
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "I",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "i",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{alphabet}}",
            "ï"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{alphabet}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "ï",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "i"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{pronoun}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "really",
      "text": "really",
      "solution": "really",
      "tokens": [
        {
          "text": "really",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{intensifier}}"
          ],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{intensifier}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "love",
      "text": "love",
      "solution": "love",
      "tokens": [
        {
          "text": "love",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:love}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{stem:love}}",
          "topics": {
            "0": 2,
            "4": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "scrambled",
      "text": "{{breakfast_food}}",
      "solution": "{{breakfast_food}}",
      "tokens": [
        {
          "text": "scrambled",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "scrambled eggs",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [
            "{{breakfast_food}}"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{breakfast_food}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "eggs.",
      "text": "",
      "solution": "{{breakfast_food}}",
      "tokens": [
        {
          "text": "eggs.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "eggs",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{breakfast_food}}",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "scrambled eggs",
          "topics": {
            "0": 2
          },
          "language": "en",
          "altSpellings": [
            "{{breakfast_food}}"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "They",
      "text": "they",
      "solution": "they",
      "tokens": [
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "They",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "they",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "are",
      "text": "are",
      "solution": "are",
      "tokens": [
        {
          "text": "are",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{stem:be}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{stem:be}}",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "the",
      "text": "the",
      "solution": "the",
      "tokens": [
        {
          "text": "the",
          "topics": {
            "0": 1
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    },
    {
      "original": "Shiiiiit.",
      "text": "shit",
      "solution": "shit",
      "tokens": [
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "Shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiiiiit.",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiiiiit",
          "topics": {
            "0": 4
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shiit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [
            "shit"
          ],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "shit",
          "topics": {
            "0": 5,
            "5": 5
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [
            "{{strong_bully_word}}"
          ],
          "leetMappings": [],
          "flags": [
            "COMMON"
          ],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        },
        {
          "text": "{{strong_bully_word}}",
          "topics": {
            "0": 3,
            "1": 0
          },
          "language": "en",
          "altSpellings": [],
          "altSenses": [],
          "leetMappings": [],
          "flags": [],
          "taskIds": [],
          "dateCreated": 0,
          "dateUpdated": 0,
          "clientId": 0
        }
      ]
    }
  ]
}
  `);
}
