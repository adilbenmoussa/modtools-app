import {Component, Input, OnInit} from '@angular/core';
import {ChatView, EnrichedTextClassifiedOutput, PolicyGuideCondition, PolicyGuide} from '../../filter-quality-interfaces';
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons';
import {getChatViewObj} from '../../../utils/audit-rules-util';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.less']
})
export class AnalysisComponent implements OnInit {

  @Input() textClassifiedOutput: EnrichedTextClassifiedOutput;
  globalChatView: ChatView;
  privateChatView: ChatView;
  globalConditions: PolicyGuideCondition[] = [
    {
      riskLevelId: 0,
      topicRateId: 6
    },
    {
      riskLevelId: 5,
      topicRateId: 5
    }
  ];

  privateCondition: PolicyGuideCondition[] = [
    {
      riskLevelId: 5,
      topicRateId: 6
    }
  ];


  constructor() {
    library.add(fas);
  }

  ngOnInit() {
    const getChatViewObjFunc = getChatViewObj(this.textClassifiedOutput.enrichedTopics);
    this.globalChatView = getChatViewObjFunc(PolicyGuide.GlobalChat, this.globalConditions);
    this.privateChatView = getChatViewObjFunc(PolicyGuide.PrivateChat, this.privateCondition);
  }
}
