import {isFunction, forEach} from 'lodash';
import {Subscription} from 'rxjs';

/**
 * Decorator helping unsubscribing the rxjs subscriptions automagically
 * @author: Adil Ben Moussa <https://github.com/adilbenmoussa>
 * @license: MIT
 *
 * How to:
 * @Unsubscribable()
 * class TestComponent implements ngOnDestroy {
 *  someMethod(){
 *   someObservableReturningASubscription()
 *   .unsubscribeOnDestroy(this); // this reference here to the component context
 *  }
 * }
 *
 *  We are working in AOT mode, so this method must be present when adding this decorator, even if empty!
 *  Otherwise 'ng build --prod' will optimize away any calls to ngOnDestroy.
 *  even if the method is added by the @Unsubscribable() decorator
 *  ngOnDestroy(){}
 */

export function Unsubscribable(): ClassDecorator {
  return function (constructor: any) {
    const originalInit = constructor.prototype.ngOnInit;
    const originalDestroy = constructor.prototype.ngOnDestroy;

    if (!isFunction(originalDestroy)) {
      console.error(`${constructor.name} is using @Unsubscribable() but does not implement ngOnDestroy, this wont work on AOT mode!`);
    }

    constructor.prototype.ngOnInit = function () {
      this.disposeBag = this.disposeBag || [];
      if (isFunction(originalInit)) {
        originalInit.apply(this, arguments);
      }
    };

    constructor.prototype.ngOnDestroy = function () {
      forEach(this.disposeBag, (disposable: Subscription) => {
        disposable.unsubscribe();
      });
      delete this.disposeBag;

      if (isFunction(originalDestroy)) {
        originalDestroy.apply(this, arguments);
      }
    };
  };
}
